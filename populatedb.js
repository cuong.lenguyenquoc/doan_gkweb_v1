#! /usr/bin/env node

console.log('This script populates some test Items, authors, genres and Iteminstances to your database. Specified database as argument - e.g.: populatedb mongodb+srv://cooluser:coolpassword@cluster0-mbdj7.mongodb.net/local_library?retryWrites=true');

// Get arguments passed on command line
var userArgs = process.argv.slice(2);
/*
if (!userArgs[0].startsWith('mongodb')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}
*/
var async = require('async')
var Item = require('./models/items')


var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var curday = function(sp){
  today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //As January is 0.
  var yyyy = today.getFullYear();
  
  if(dd<10) dd='0'+dd;
  if(mm<10) mm='0'+mm;
  return (mm+sp+dd+sp+yyyy);
  };

var Items = []

function ItemCreate(name, type, old_price, new_price, import_price, created_at, updated_at, description, image, cb) {
  item_detail = { 
    name: name,
    old_price: old_price,
    new_price: new_price,
    import_price: import_price,
    created_at: created_at,
    updated_at: updated_at,
    description: description,
    image: image
  }
  if (type != false) item_detail.type = type
    
  var NewItem = new Item(item_detail);    
  NewItem.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New Item: ' + NewItem);
    Items.push(NewItem)
    cb(null, NewItem)
  }  );
}



function createItems(cb) {
    async.parallel([
        function(callback) {
          ItemCreate('Áo khoắc Đen', 'mensJacket', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo analog', 'mensJacket', 500000, 350000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 3', 'mensJacket', 460000, 360000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 4', 'mensJacket', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 5', 'mensJacket', 490000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 6', 'mensJacket', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 7', 'mensJacket', 600000, 380000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 8', 'mensJacket', 550000, 330000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 9', 'mensJacket', 490000, 370000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 10', 'mensJacket', 580000, 4600000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jacket/mens_jacket10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Áo thun 1', 'mensTshirt', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 2', 'mensTshirt', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 3', 'mensTshirt', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 4', 'mensTshirt', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 5', 'mensTshirt', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 6', 'mensTshirt', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 7', 'mensTshirt', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 8', 'mensTshirt', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 9', 'mensTshirt', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 10', 'mensTshirt', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_tshirt/mens_tshirt10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Áo sơ mi 1', 'mensShirt', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 2', 'mensShirt', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 3', 'mensShirt', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 4', 'mensShirt', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 5', 'mensShirt', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 6', 'mensShirt', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 7', 'mensShirt', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 8', 'mensShirt', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 9', 'mensShirt', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 10', 'mensShirt', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_shirt/mens_shirt10.jpg', callback);
        },
        
        function(callback) {
          ItemCreate('Quần jean 1', 'mensJeans', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 2', 'mensJeans', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 3', 'mensJeans', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 4', 'mensJeans', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 5', 'mensJeans', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 6', 'mensJeans', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 7', 'mensJeans', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 8', 'mensJeans', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 9', 'mensJeans', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 10', 'mensJeans', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_jeans/mens_jeans10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Quần short 1', 'mensShort', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 2', 'mensShort', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 3', 'mensShort', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 4', 'mensShort', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 5', 'mensShort', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 6', 'mensShort', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 7', 'mensShort', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 8', 'mensShort', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 9', 'mensShort', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 10', 'mensShort', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_short/mens_short10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Quần tây 1', 'mensTrousers', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 2', 'mensTrousers', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 3', 'mensTrousers', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 4', 'mensTrousers', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 5', 'mensTrousers', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 6', 'mensTrousers', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 7', 'mensTrousers', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 8', 'mensTrousers', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 9', 'mensTrousers', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 10', 'mensTrousers', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_trousers/mens_trousers10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Ví 1', 'mensWallet', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 2', 'mensWallet', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 3', 'mensWallet', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 4', 'mensWallet', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 5', 'mensWallet', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 6', 'mensWallet', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 7', 'mensWallet', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 8', 'mensWallet', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 9', 'mensWallet', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 10', 'mensWallet', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_wallet/mens_wallet10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Đồng hồ 1', 'mensWatch', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  2', 'mensWatch', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  3', 'mensWatch', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  4', 'mensWatch', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  5', 'mensWatch', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  6', 'mensWatch', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  7', 'mensWatch', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  8', 'mensWatch', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  9', 'mensWatch', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/mens_watch/mens_watch9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  10', 'mensWatch', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '.../images/mens_watch/mens_watch10.jpg.jpg', callback);
        },

        function(callback) {
          ItemCreate('Áo khoắc Đen', 'womensJacket', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo analog', 'womensJacket', 500000, 350000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 3', 'womensJacket', 460000, 360000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 4', 'womensJacket', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 5', 'womensJacket', 490000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 6', 'womensJacket', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 7', 'womensJacket', 600000, 380000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 8', 'womensJacket', 550000, 330000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 9', 'womensJacket', 490000, 370000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo 10', 'womensJacket', 580000, 4600000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jacket/womens_jacket10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Áo thun 1', 'womensTshirt', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 2', 'womensTshirt', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 3', 'womensTshirt', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 4', 'womensTshirt', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 5', 'womensTshirt', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 6', 'womensTshirt', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 7', 'womensTshirt', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 8', 'womensTshirt', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 9', 'womensTshirt', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo thun 10', 'womensTshirt', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_tshirt/womens_tshirt10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Áo sơ mi 1', 'womensShirt', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 2', 'womensShirt', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 3', 'womensShirt', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 4', 'womensShirt', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 5', 'womensShirt', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 6', 'womensShirt', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 7', 'womensShirt', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 8', 'womensShirt', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 9', 'womensShirt', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Áo sơ mi 10', 'womensShirt', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_shirt/womens_shirt10.jpg', callback);
        },
        
        function(callback) {
          ItemCreate('Quần jean 1', 'womensJeans', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 2', 'womensJeans', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 3', 'womensJeans', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 4', 'womensJeans', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 5', 'womensJeans', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 6', 'womensJeans', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 7', 'womensJeans', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 8', 'womensJeans', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 9', 'womensJeans', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần jean 10', 'womensJeans', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_jeans/womens_jeans10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Quần short 1', 'womensShort', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 2', 'womensShort', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 3', 'womensShort', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 4', 'womensShort', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 5', 'womensShort', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 6', 'womensShort', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 7', 'womensShort', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 8', 'womensShort', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 9', 'womensShort', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần short 10', 'womensShort', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_short/womens_short10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Quần tây 1', 'womensTrousers', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 2', 'womensTrousers', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 3', 'womensTrousers', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 4', 'womensTrousers', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 5', 'womensTrousers', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 6', 'womensTrousers', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 7', 'womensTrousers', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 8', 'womensTrousers', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 9', 'womensTrousers', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Quần tây 10', 'womensTrousers', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_trousers/womens_trousers10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Ví 1', 'womensWallet', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 2', 'womensWallet', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 3', 'womensWallet', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 4', 'womensWallet', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 5', 'womensWallet', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 6', 'womensWallet', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 7', 'womensWallet', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 8', 'womensWallet', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 9', 'womensWallet', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Ví 10', 'womensWallet', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_wallet/womens_wallet10.jpg', callback);
        },

        function(callback) {
          ItemCreate('Đồng hồ 1', 'womensWatch', 450000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch1.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  2', 'womensWatch', 250000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch2.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  3', 'womensWatch', 500000, 250000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch3.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  4', 'womensWatch', 600000, 500000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch4.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  5', 'womensWatch', 150000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch5.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  6', 'womensWatch', 340000, 200000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch6.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  7', 'womensWatch', 120000, 60000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch7.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  8', 'womensWatch', 310000, 270000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch8.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  9', 'womensWatch', 360000, 300000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '../images/womens_watch/womens_watch9.jpg', callback);
        },
        function(callback) {
          ItemCreate('Đồng hồ  10', 'womensWatch', 140000, 120000, 100000, curday("-"), curday("-") , 'Được thiết kế dựa trên nền chất liệu cao cấp Đế chống trơn trượt, kết hợp cùng kiểu dáng thể thao và màu sắc nổi bật, chắc chắn sẽ là phụ kiện được nhiều bạn lựa chọn để thể hiện phong cách thể thao khỏe khoắn, năng động. Bên cạnh đó, chất liệu nhẹ, êm ái giúp bạn luôn cảm thấy thoải mái trong suốt quá trình vận động.', '.../images/womens_watch/womens_watch1.jpg.jpg', callback);
        },



        ],
        // optional callback
        cb);
}


async.series([
    createItems,
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    else {
        console.log('ItemInstances: '+Items);
        
    }
    // All done, disconnect from database
    mongoose.connection.close();
});



