var Item = require('../models/items');

exports.index = function(req, res) {
    Item.find()
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let userID = null
        if (req.session.passport) {
        userID = req.session.passport.user
        }
        res.render('mens/clothing', { title: 'Thời trang nam',  list_items: list_items, userID: userID });
      });
};

exports.jacket = function(req, res, next) {
    Item.find({type:'mensJacket'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensjacket', { title: 'Áo Khoác',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.jean = function(req, res) {
    Item.find({type:'mensJeans'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensJeans', { title: 'Quần jeans',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.shirt = function(req, res) {
    Item.find({type:'mensShirt'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensShirt', { title: 'Áo sơ mi',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.trouser = function(req, res) {
    Item.find({type:'mensTrousers'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensTrousers', { title: 'Quần tây',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.tshirt = function(req, res) {
    Item.find({type:'mensTshirt'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensTshirt', { title: 'Áo thun',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.wallet = function(req, res) {
    Item.find({type:'mensWallet'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensWallet', { title: 'Ví',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.watch = function(req, res) {
    Item.find({type:'mensWatch'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensWatch', { title: 'Đồng hồ',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.short = function(req, res) {
    Item.find({type:'mensShort'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('mens/mensShort', { title: 'Quần short',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};