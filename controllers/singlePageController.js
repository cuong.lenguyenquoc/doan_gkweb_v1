var Item = require('../models/items');
var User = require('../models/user');
var Comment = require('../models/comments')
const { ObjectId } = require('mongodb');

var curday = function(sp){
	today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //As January is 0.
	var yyyy = today.getFullYear();
	
	if(dd<10) dd='0'+dd;
	if(mm<10) mm='0'+mm;
	return (mm+sp+dd+sp+yyyy);
};

exports.index = function (req, res) {
  Item.find({ _id: ObjectId(req.query.id) })
    .exec(function (err, items) {
      if (err) { return next(err); }
      var item = items[0]

      Item.find({ type: item.type })
        .exec(function (err, related_item) {
          if (err) { return next(err); }
          if (related_item.length > 4) {
            var length_related_item = related_item.length
            var temp = 0
            do {
              if (related_item[temp]._id == req.query.id) {
                related_item.splice(temp, 1)
                break
              }
              temp++
            } while (true)
            length_related_item = length_related_item - 1
            for (var j = 0; j < length_related_item - 4; j++) {
              related_item.splice(Math.floor(Math.random() * related_item.length), 1);
            }
          }
          let userID = null
          let name = null
          let comments = null
          let i = null
          let n = null
          if (req.session.passport) {
            userID = req.session.passport.user
          }

          Comment.find({ id_product: ObjectId(req.query.id) })
            .exec(function (err, comments) {
              if (err) { return next(err); }
              comments = comments
              comments.reverse()
              //phan trang comment
              let pages = parseInt(comments.length / 4)
              if (comments.length % 4 > 0) {
                pages = pages + 1
              }
              var page = req.query.pagecomment
              if (page > pages) {
                page = pages
              } else if (page < 1) {
                page = 1
              }
              i = 4 * (page - 1)
              var n
              if (pages == page && comments.length%4 != 0) {
                n = 4 * (page - 1) + comments.length % 4
              }
              else {
                n = 4 * page
              }
              if (userID != null) {

                User.findOne({ _id: ObjectId(req.session.passport.user) })
                  .exec(function (err, user) {
                    if (err) { return next(err); }
                    name = user.information.name
                    res.render('single', { title: 'Chi tiết sản phẩm', item: item, userID: userID, related_item: related_item, name: name, comments: comments, i: i, n: n, page: page, pages: pages });
                  });
              }
              else {
                res.render('single', { title: 'Chi tiết sản phẩm', item: item, userID: userID, related_item: related_item, name: name, comments: comments, i: i, n: n, page: page, pages: pages });
              }
            });
        });
    });
}

exports.addComment = function (req, res) {
  console.log(req.body.idProduct)
  var comment = new Comment( { id_product:req.body.idProduct, user_name:req.body.Name, date:curday("-"), message:req.body.Message} );
  comment.save(function (err) {
    if (err) { return next(err); }
        //successful - redirect to new book record.
        res.redirect('/single?id='+req.body.idProduct+'&pagecomment=1');
    });
}