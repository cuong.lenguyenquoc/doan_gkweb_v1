var Item = require('../models/items');

exports.index = function(req, res) {
    Item.find()
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let mens_items = list_items.filter(function(item){
            return item.type.indexOf("women") === -1
        })
        let womens_items = list_items.filter(function(item){
            return item.type.indexOf("women") !== -1
        })
        let watch_items = list_items.filter(function(item){
            return item.type.indexOf("Watch") !== -1
        })
        let wallet_items = list_items.filter(function(item){
            return item.type.indexOf("Wallet") !== -1
        })
        let userID = null
        if (req.session.passport) {
            userID = req.session.passport.user
        }
        console.log(userID)
        res.render('index', { title: 'Trang chủ',  mens_items: mens_items, womens_items: womens_items, watch_items: watch_items, wallet_items: wallet_items, userID: userID });
      });
};

exports.cart = function(req, res) {
    let userID = null
        if (req.session.passport) {
            userID = req.session.passport.user
        }
    res.render('cart', { title: 'Trang chủ', userID: userID });
};