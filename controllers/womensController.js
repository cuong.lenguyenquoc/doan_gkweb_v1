var Item = require('../models/items');

exports.index = function(req, res) {
    Item.find()
      .exec(function (err, items) {
        if (err) { return next(err); }
        let list_items = items.filter(function(item){
          return item.type.indexOf("women") !== -1
      })
        //Successful, so render
        let userID = null
        if (req.session.passport) {
            userID = req.session.passport.user
        }
        res.render('womens/womensClothing', { title: 'Thời trang nữ',  list_items: list_items, userID: userID });
      });
};

exports.jacket = function(req, res, next) {
    Item.find({type:'womensJacket'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensJacket', { title: 'Áo Khoác',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.jean = function(req, res) {
    Item.find({type:'womensJeans'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensJeans', { title: 'Quần Jeans',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID });
      });
};

exports.shirt = function(req, res) {
    Item.find({type:'womensShirt'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensShirt', { title: 'Áo Sơ Mi',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.trouser = function(req, res) {
    Item.find({type:'womensTrousers'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page&& list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensTrousers', { title: 'Quần Tây',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.tshirt = function(req, res) {
    Item.find({type:'womensTshirt'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensTshirt', { title: 'Áo Thun',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.wallet = function(req, res) {
    Item.find({type:'womensWallet'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensWallet', { title: 'Ví Nữ',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.watch = function(req, res) {
    Item.find({type:'womensWatch'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensWatch', { title: 'Đồng Hồ Nữ',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};

exports.short = function(req, res) {
    Item.find({type:'womensShort'})
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
        var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      res.render('womens/womensShort', { title: 'Quần short',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID });
      });
};