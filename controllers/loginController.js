
exports.index = function(req, res) {
    var messages = req.flash('error');
    var dataFormArray = req.flash('dataForm');
  dataForm = {
              email : '',
              password : ''
  }
  let userID = null
    if (req.session && req.session.passport) {
        userID = req.session.passport.user
    } 
  if (dataFormArray.length > 0) {
    dataForm.email = dataFormArray[0].email
    dataForm.password = dataFormArray[0].password
  }   
  res.render('users/login',{
      messages: messages,
      hasErrors: messages.length > 0,
      dataForm : dataForm,
      userID: userID
  });
}