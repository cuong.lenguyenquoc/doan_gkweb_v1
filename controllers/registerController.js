
exports.index = function(req, res) {
    var messages = req.flash('error');
    var dataFormArray = req.flash('dataForm');
    dataForm = {
      name : '',
      phoneNumber: '',
      email : '',
      password : ''
    }
  if (dataFormArray.length > 0) {
    dataForm.name = dataFormArray[0].name
    dataForm.phoneNumber = dataFormArray[0].phoneNumber
    dataForm.email = dataFormArray[0].email
    dataForm.password = dataFormArray[0].password
  }
  let userID = null
    if (req.session.passport) {
        userID = req.session.passport.user
    } 
  res.render('users/register',{
      messages: messages,
      hasErrors: messages.length > 0,
      dataForm : dataForm,
      userID: userID
  });
}