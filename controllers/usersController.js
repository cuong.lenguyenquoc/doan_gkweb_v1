var User = require('../models/user');
const {ObjectId} = require('mongodb');

exports.profile = function(req, res) {
    User.findOne({_id: ObjectId(req.session.passport.user)})
      .exec(function (err, user) {
        if (err) { return next(err); }
        var messages = req.flash('statusUpdate');
        
        res.render('users/profile', { 
            title: 'Thông tin tài khoản', 
            user: user, 
            messages: messages,
            hasErrors: messages.length > 0,
            userID: user._id});
      });
};

exports.logout = function(req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function(err) {
          if(err) {
            return next(err);
          } else {
            return res.redirect('/');
          }
        });
      }
};

exports.checkUpdatedData = function(req, res, next) {
    // form values
    var name = req.body.name;
    var phoneNumber = req.body.phoneNumber
    //check form validation
    req.checkBody("name", "name is required").notEmpty();
    req.checkBody("phoneNumber", "Vui lòng điền số điện thoại đúng định dạng").isMobilePhone()
    //check for errors
    var errors = req.validationErrors();

    if (errors) {
        var messages = [];
        errors.forEach(function(error){
            messages.push(error.msg);
        });
        req.flash('statusUpdate',messages)
        res.redirect('/users/profile')
    } else {
        next()
    }
}

exports.update = function(req, res, next) {
    User.findOne({_id: ObjectId(req.session.passport.user)})
      .exec(function (err, user) {
        if (err) { return next(err); }
        user.information.name = req.body.name
        user.information.phoneNumber = req.body.phoneNumber
        user.save();
        var messages = [];
        messages.push("Cập nhật thông tin thành công");
        req.flash('statusUpdate',messages)
        res.redirect('/users/profile')
      });
};

exports.checkResetPasswordData = function(req, res, next) {
    User.findOne({_id: ObjectId(req.session.passport.user)})
      .exec(function (err, user) {
        if (err) { return next(err); }
            // form values
            var old_password = req.body.old_password;
            var new_password = req.body.new_password
            var confirm_password = req.body.confirm_password
            //check form validation
            var messages = [];
            if (!user.validPassword(old_password)) {
                messages.push("Mật khẩu cũ không chính xác, vui lòng nhập lại") 
            }
            if (new_password != confirm_password) {
                messages.push("Xác nhận mật khẩu không trùng, vui lòng nhập lại")
            }
            if (messages.length > 0) {
                res.render('users/resetPassword', {   
                    messages: messages,
                    hasErrors: messages.length > 0,
                    userID: user._id
                });
            } else {
                next()
            }
      })
}

exports.getResetPassword = function(req, res, next) {
    var messages = []
    let userID = null
    if (req.session.passport) {
        userID = req.session.passport.user
    } 
    res.render('users/resetPassword', {   
        messages: messages,
        hasErrors: messages.length > 0,
        userID: userID
    });
};

exports.postResetPassword = function(req, res, next) {
    User.findOne({_id: ObjectId(req.session.passport.user)})
      .exec(function (err, user) {
        if (err) { return next(err); }
        user.password = user.generateHash(req.body.new_password);
        user.save();
        if (req.session) {
            // delete session object
            req.session.destroy(function(err) {
              if(err) {
                return next(err);
              } else {
                dataForm = {
                    email : user.email,
                    password : ''
                }
                var messages = [];
                messages.push("Đổi mật khẩu thành công");
                res.render('users/login',{
                    messages: messages,
                    hasErrors: messages.length > 0,
                    dataForm : dataForm,
                    userID: null
                });
              }
            });
          }
        
      });
};