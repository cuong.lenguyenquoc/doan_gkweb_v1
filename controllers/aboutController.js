
exports.index = function(req, res) {
    let userID = null
    if (req.session.passport) {
        userID = req.session.passport.user
    }
    res.render('about', {title: 'About', userID: userID })
};