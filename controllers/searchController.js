var Item = require('../models/items');

exports.search = function(req, res, next) {
    Item.find()
      .exec(function (err, list_items) {
        if (err) { return next(err); }
        //Successful, so render
        var name = req.query.name
        for(var i= 0; i < list_items.length; i++){
          var check = list_items[i].name.toUpperCase().search(name.toUpperCase())
          if(check == -1){
            list_items.splice(i,1)
            i--
          }
        }
        let pages =parseInt( list_items.length / 9 )
        if(list_items.length % 9 > 0){
          pages = pages + 1
        }
         var page = req.query.page
        if (page > pages) {
            page = pages
        } else if (page < 1) {
            page = 1
        }
        var i = 9 * (page - 1) 
        var n 
        if(pages == page && list_items.length%9 != 0){
        n = 9 * (page-1) + list_items.length % 9} 
        else{ 
        n= 9 * page
      } 
      let number = n - i 
      let row = parseInt(number / 3)
      if(number % 3 >0 ){
        row = row +1
      }
      let col = number % 3
      if(col == 0){
        col = 3
      }
      let col1
      if(row >= 2){
        col1 = 3
      }else{
        col1 = col
      }
      let col2
      if(row == 3){
        col2 = 3
      }else{
        col2 = col
      }
      let i1 = i
      let i2 = i + 3
      let i3 = i2 + 3
      let userID = null
      if (req.session.passport) {
          userID = req.session.passport.user
      }
      console.log(list_items.length)
      console.log(n)
      res.render('searchResult', { title: 'Kết quả',  list_items: list_items, page: page, pages:pages, i:i, n:n, row:row, col:col, i1:i1, i2:i2, i3:i3, col1: col1, col2:col2, userID: userID, name:name });
      });
};

