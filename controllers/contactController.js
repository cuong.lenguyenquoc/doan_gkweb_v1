
exports.index = function(req, res) {
    let userID = null
    if (req.session.passport) {
        userID = req.session.passport.user
    }
    res.render('contact', {title: 'Contact', userID: userID})
};