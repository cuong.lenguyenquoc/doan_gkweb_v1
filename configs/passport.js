var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

   //Passport register
passport.use('local-register', new LocalStrategy({
    usernameField : 'email',
    passswordField : 'password',
    passReqToCallback : true
},function(req, email, password, done){
    User.findOne({
      'email' : email       
  }, function(err, user){
      if(err){
          return done(err)
      }
      if(user){
        req.flash('dataForm',dataForm)
          return done(null, false, {
              message : 'Email đã được sử dụng, vui lòng chọn email khác'    
          })
      }
      var newUser = new User();
      newUser.information.name = req.body.name
      newUser.information.phoneNumber = req.body.phoneNumber
      newUser.email = email;
      newUser.password = newUser.generateHash(password);
      newUser.isBlocked = false;
      newUser.save(function(err, result) {
          if (err) {
              return done(err);
          } else {                    
            return done(null, newUser);            
          }
      });
  })
}));

/* Passport login */
passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, email, password, done) {
    User.findOne({
        'email': email
    }, function(err, user) {
        if (err) {
            return done(err);
        }

        if (!user) {
            req.flash('dataForm',dataForm)
            return done(null, false, {
                message: 'Tài khoản này không tồn tại, vui lòng kiểm tra lại.'
            });
        }
        if (!user.validPassword(password)) {
            req.flash('dataForm',dataForm)
            return done(null, false, {
                message: 'Mật khẩu không đúng, vui lòng kiểm tra lại.'
            });
        }
        if (user.isBlocked) {
            req.flash('dataForm',dataForm)
            return done(null, false, {
                message: 'Tài khoản này đã bị tạm khoá, vui lòng liên hệ với admin để mở khoá.'
            });
        }
        return done(null, user);
    });
}));