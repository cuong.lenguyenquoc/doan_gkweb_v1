var express = require('express');
var router = express.Router();
var mensController = require('../controllers/mensController');
var womensController = require('../controllers/womensController');
var singlePageContoller = require('../controllers/singlePageController')
var aboutContoller = require('../controllers/aboutController');
var contactContoller = require('../controllers/contactController');
var indexController = require('../controllers/indexController');
var searchController = require('../controllers/searchController');

/* GET home page. */
router.get('/', indexController.index);

router.get('/about', aboutContoller.index);

router.get('/contact', contactContoller.index)

router.get('/cart', indexController.cart);

router.get('/search', searchController.search)

router.get('/mensclothing', mensController.index) 

router.post('/addcomment', singlePageContoller.addComment)

router.get('/mens/mensjacket', mensController.jacket) 

router.get('/mens/menstshirt', mensController.tshirt) 

router.get('/mens/mensshirt', mensController.shirt) 

router.get('/mens/mensshort', mensController.short) 

router.get('/mens/mensjeans', mensController.jean) 

router.get('/mens/menstrousers', mensController.trouser) 

router.get('/mens/menswallet', mensController.wallet) 

router.get('/mens/menswatch', mensController.watch) 


router.get('/womensclothing', womensController.index) 

router.get('/womens/womensjacket', womensController.jacket) 

router.get('/womens/womenstshirt', womensController.tshirt) 

router.get('/womens/womensshirt', womensController.shirt) 

router.get('/womens/womensshort', womensController.short) 

router.get('/womens/womensjeans', womensController.jean) 

router.get('/womens/womenstrousers', womensController.trouser) 

router.get('/womens/womenswallet', womensController.wallet) 

router.get('/womens/womenswatch', womensController.watch) 

router.get('/single', singlePageContoller.index) 

module.exports = router;
