var express = require('express');
var router = express.Router();
var passport = require('passport');

var loginController = require('../controllers/loginController');
var registerController = require('../controllers/registerController');
var usersController = require('../controllers/usersController');
var middleware = require('../middleware/middleware');

/* GET users listing. */
router.get('/login', loginController.index)

router.route('/login').post(function (req, res, next) {
    // form values
    var email = req.body.email;
    var password = req.body.password;
    //check form validation
    req.checkBody("email", "email is invalid").isEmail();
    req.checkBody("password", "password is required").notEmpty();
    //check for errors
    var errors = req.validationErrors();
  
    dataForm = {
        email : email,
        password : password
    }
    if (errors) {
        var messages = [];
        errors.forEach(function(error){
            messages.push(error.msg);
        });
        let userID = null
        if (req.session && req.session.passport) {
            userID = req.session.passport.user
        } 
        res.render('users/login',{
            messages: messages,
            hasErrors: messages.length > 0,
            dataForm : dataForm,
            userID: userID
        });
    } else {
       next()
    }
  }, passport.authenticate('local-login',{
        successRedirect:'/',
        failureRedirect:'/users/login',
        failureFlash : true  
  }))
  
router.get('/profile', middleware.requiresLogin, usersController.profile)

router.get('/register', registerController.index)

router.route('/register').post(function (req, res, next) {
  // form values
  var name = req.body.name;
  var phoneNumber = req.body.phoneNumber
  var email = req.body.email;
  var password = req.body.password;
  //check form validation
  req.checkBody("name", "name is required").notEmpty();
  req.checkBody("email", "email is invalid").isEmail();
  req.checkBody("password", "password is required").notEmpty();
  req.checkBody('password', 'Xác nhận mật khẩu không giống nhau, vui lòng kiểm tra lại.').equals(req.body.confirm_password);
  req.checkBody("phoneNumber", "Vui lòng điền số điện thoại đúng định dạng").isMobilePhone()
  //check for errors
  var errors = req.validationErrors();

  dataForm = {
      name : name,
      email : email,
      password : password,
      phoneNumber: phoneNumber
  }
  let userID = null
        if (req.session && req.session.passport) {
            userID = req.session.passport.user
        } 
  if (errors) {
      var messages = [];
      errors.forEach(function(error){
          messages.push(error.msg);
      });
      res.render('users/register',{
          messages: messages,
          hasErrors: messages.length > 0,
          dataForm : dataForm,
          userID: userID
      });
  } else {
     next()
  }
}, passport.authenticate('local-register',{
      successRedirect:'/users/profile',
      failureRedirect:'/users/register',
      failureFlash : true  
}))

router.get('/logout', usersController.logout);
router.post('/update', usersController.checkUpdatedData, usersController.update)
router.get('/reset-password',middleware.requiresLogin, usersController.getResetPassword);
router.post('/reset-password', usersController.checkResetPasswordData, usersController.postResetPassword)
module.exports = router;
