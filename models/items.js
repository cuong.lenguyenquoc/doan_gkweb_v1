var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ItemSchema = new Schema(
  {
    name: {type: String, required: true, max: 100},
    type: {type: String, required: true, enum: ['mensJacket', 'mensJeans', 'mensShirt', 'mensShort', 'mensTrousers', 'mensTshirt', 'mensWallet', 'mensWatch', 'womensJacket', 'womensJeans', 'womensShirt', 'womensShort', 'womensTrousers', 'womensTshirt', 'womensWallet', 'womensWatch'], default: 'womensJacket'},
    old_price: {type: Number, required: true},
    new_price: {type: Number, required: true},
    description: {type: String},
    image: {type:String, required: true},
    import_price: {type: Number, required: true},
    created_at: {type: String, required: true},
    updated_at: {type: String, required: true}
  }
);


// Phương thức ảo cho URL của tác giả
ItemSchema
.virtual('url')
.get(function () {
  return '/single?id=' + this._id + '&pagecomment=1';
});

//xuất mô hình
module.exports = mongoose.model('Item', ItemSchema);