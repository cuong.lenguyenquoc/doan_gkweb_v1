var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var OrderSchema = new Schema(
  {
    idUser: {type: String, required: true},
    idItems: {type: Array, required: true},
    created_at: {type: String, required: true},
  }
);


module.exports = mongoose.model('Order', OrderSchema);