var mongoose = require('mongoose');
var bcrypt   = require('bcryptjs');

var UserSchema = mongoose.Schema({
    email: String,
    password: String,
    isBlocked: Boolean,
    information: {
       name: String,
       phoneNumber: String
    }
});


UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);
