var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CommentSchema = new Schema(
  {
    id_product: String,
    user_name: String,
    date: String,
    message: String,
  }
);


//xuất mô hình
module.exports = mongoose.model('Comment', CommentSchema);